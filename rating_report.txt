Creating test database for alias 'default' ('file:memorydb_default?mode=memory&cache=shared')...
test_product_rating (rating.tests.RateViewTest) ... ok

----------------------------------------------------------------------
Ran 1 test in 0.723s

OK
Destroying test database for alias 'default' ('file:memorydb_default?mode=memory&cache=shared')...
Operations to perform:
  Synchronize unmigrated apps: messages, rest_framework, staticfiles
  Apply all migrations: admin, auth, authtoken, category, contenttypes, inventory, product, rating, sessions, users
Synchronizing apps without migrations:
  Creating tables...
    Running deferred SQL...
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying auth.0009_alter_user_last_name_max_length... OK
  Applying auth.0010_alter_group_name_max_length... OK
  Applying auth.0011_update_proxy_permissions... OK
  Applying auth.0012_alter_user_first_name_max_length... OK
  Applying authtoken.0001_initial... OK
  Applying authtoken.0002_auto_20160226_1747... OK
  Applying authtoken.0003_tokenproxy... OK
  Applying category.0001_initial... OK
  Applying product.0001_initial... OK
  Applying product.0002_product_category_set... OK
  Applying product.0003_auto_20210307_1808... OK
  Applying inventory.0001_initial... OK
  Applying rating.0001_initial... OK
  Applying rating.0002_auto_20210313_1906... OK
  Applying sessions.0001_initial... OK
  Applying users.0001_initial... OK
System check identified no issues (0 silenced).
{'template_name': None, 'context_data': None, 'using': None, '_post_render_callbacks': [], '_request': None, '_headers': {'content-type': ('Content-Type', 'application/json'), 'vary': ('Vary', 'Accept'), 'allow': ('Allow', 'POST, OPTIONS'), 'x-frame-options': ('X-Frame-Options', 'DENY'), 'content-length': ('Content-Length', '23'), 'x-content-type-options': ('X-Content-Type-Options', 'nosniff'), 'referrer-policy': ('Referrer-Policy', 'same-origin')}, '_resource_closers': [], '_handler_class': None, 'cookies': <SimpleCookie: >, 'closed': True, 'status_code': 404, '_reason_phrase': None, '_charset': None, '_container': [b'{"detail":"Not found."}'], '_is_rendered': True, 'data': {'detail': ErrorDetail(string='Not found.', code='not_found')}, 'exception': True, 'content_type': None, 'accepted_renderer': <rest_framework.renderers.JSONRenderer object at 0x7f2cd60962e0>, 'accepted_media_type': 'application/json', 'renderer_context': {'view': <rating.views.RatingView object at 0x7f2cd610bf70>, 'args': (), 'kwargs': {}, 'request': <rest_framework.request.Request: POST '/api/rating/'>, 'response': <Response status_code=404, "application/json">}, '_has_been_logged': True, 'wsgi_request': <WSGIRequest: POST '/api/rating/'>, 'exc_info': None, 'client': <rest_framework.test.APIClient object at 0x7f2cd68bcac0>, 'request': {'PATH_INFO': '/api/rating/', 'REQUEST_METHOD': 'POST', 'SERVER_PORT': '80', 'wsgi.url_scheme': 'http', 'CONTENT_LENGTH': '55', 'CONTENT_TYPE': 'application/json', 'wsgi.input': <django.test.client.FakePayload object at 0x7f2cd610bbb0>, 'QUERY_STRING': '', 'HTTP_AUTHORIZATION': 'Token 9f39898a5dc19760338ab6ed4fc1ed73e34a8fa1'}, 'templates': [], 'context': None, 'json': functools.partial(<bound method ClientMixin._parse_json of <rest_framework.test.APIClient object at 0x7f2cd68bcac0>>, <Response status_code=404, "application/json">), 'resolver_match': <SimpleLazyObject: <function Client.request.<locals>.<lambda> at 0x7f2cd60f2700>>, '_dont_enforce_csrf_checks': True}
