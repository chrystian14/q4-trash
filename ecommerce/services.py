from rest_framework import status as stts


def BUILD_HTTP_RESPONSE(data=None, status=None):
    HTTP_RESPONSE = {
        200: {
            'text': {'detail': 'OK'},
            'status': stts.HTTP_200_OK
        },
        201: {
            'text': {'detail': 'CREATED'},
            'status': stts.HTTP_201_CREATED
        },
        204: {
            'text': {'detail': 'NO CONTENT'},
            'status': stts.HTTP_204_NO_CONTENT
        },
        400: {
            'text': {'detail': 'BAD REQUEST'},
            'status': stts.HTTP_400_BAD_REQUEST
        },
        401: {
            'text': {'detail': 'UNAUTHORIZEED'},
            'status': stts.HTTP_401_UNAUTHORIZED
        },
        404: {
            'text': {'detail': 'NOT FOUND'},
            'status': stts.HTTP_404_NOT_FOUND
        },
        403: {
            'text': {'detail': 'FORBIDDEN'},
            'status': stts.HTTP_403_FORBIDDEN
        },
        422: {
            'text': {'detail': 'UNPROCESSABLE ENTITY'},
            'status': stts.HTTP_422_UNPROCESSABLE_ENTITY
        }
    }

    if data is not None:
        return {
            'data': data,
            'status': HTTP_RESPONSE[status]['status']
        }

    if status is None:
        return {
            'data': HTTP_RESPONSE[200]['text'],
            'status': HTTP_RESPONSE[200]['status']
        }

    return {
        'data': HTTP_RESPONSE[status]['text'],
        'status': HTTP_RESPONSE[status]['status']
    }
