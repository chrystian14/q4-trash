from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User

from product.models import Product


class Rate(models.Model):
    rating = models.FloatField(validators=[
        MaxValueValidator(5), MinValueValidator(0)
    ])
    comment = models.CharField(max_length=255)

    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
