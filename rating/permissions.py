from rest_framework.permissions import BasePermission, SAFE_METHODS


class OnlyLoggedUserCanRate(BasePermission):
    def has_permission(self, request, view):
        is_staff = view.request.user.is_staff
        is_superuser = view.request.user.is_superuser
        auth = request.auth
        if request.method in SAFE_METHODS:
            return True

        if request.method == 'delete' and auth or is_superuser:
            return True

        if is_staff or is_superuser or not auth:
            return False

        return True
