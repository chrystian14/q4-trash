from rest_framework import serializers

from django.contrib.auth.models import User

from users.serializers import UserSerializer
from product.serializers import ProductSerializer
from product.models import Product


class RatingSerializer(serializers.Serializer):
    id = serializers.PrimaryKeyRelatedField(read_only=True)

    rating = serializers.FloatField(max_value=5, min_value=0)
    comment = serializers.CharField(max_length=255)

    product_id = serializers.IntegerField()
    user_id = serializers.IntegerField(required=False)


class RatingGetSerializer(serializers.Serializer):
    def to_representation(self, data: dict):
        user = User.objects.get(id=data['user_id'])
        user_serialized = UserSerializer(user).data

        product = Product.objects.get(id=data['product_id'])
        product_serialized = ProductSerializer(product).data

        return {
            'id': data['id'],
            'rating': data['rating'],
            'username': user_serialized['username'],
            'comment': data['comment'],
            'product': product_serialized,
        }


class RatingPostSerializer(serializers.Serializer):
    def to_representation(self, data: dict, product: Product):
        serialized = ProductSerializer(product).data

        data['id'] = serialized['id']
        data['product'] = serialized
        return data


class RatingPatchSerializer(serializers.Serializer):
    comment = serializers.CharField(max_length=255)
    rating = serializers.FloatField(max_value=5, min_value=0)
