from django.test import TestCase

from rest_framework.test import APIClient


class RateViewTest(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.staff = {
            "accounts": {
                "username": "staff1",
                "password": "1234",
                "is_staff": True,
                "is_superuser": True
            },
            "login": {
                "username": "staff1",
                "password": "1234"
            }
        }

        cls.user = {
            "accounts": {
                "username": "user",
                "password": "1234",
            },
            "login": {
                "username": "user",
                "password": "1234"
            }
        }

        cls.product_data = {
            "name": "Produtinho Maneirao :DD",
            "price": 9.99,
            "description": "Um produto bem maneiro !XD",
            "categories": [
                {
                    "name": "eletronico",
                    "description": "tem fio"
                },
                {
                    "name": "infantil",
                    "description": "Facha +5 anos"
                }
            ]
        }

        cls.rate = {
            "data": {
                "rating": 5,
                "comment": "produtao bom :D",
                "product_id": 1
            },
            "response": {
                "id": 1,
                "rating": 5,
                "comment": "produtao bom :D",
                "product": {
                    "id": 1,
                    "name": "Produtinho Maneirao :DD",
                    "price": 9.99,
                    "description": "Um produto bem maneiro !XD",
                    "categories": [
                        {
                            "id": 1,
                            "name": "eletronico",
                            "description": "tem fio"
                        },
                        {
                            "id": 2,
                            "name": "infantil",
                            "description": "Facha +5 anos"
                        }
                    ]
                },
            }
        }

        cls.rate_wrong_fields = {
            "data": {
                "ratg": 5,
                "comnt": "produtao bom :D",
                "proct_id": 6
            },
            "response": {
                "rating": [
                    "This field is required."
                ],
                "comment": [
                    "This field is required."
                ],
                "product_id": [
                    "This field is required."
                ]
            }
        }

        cls.six_stars_rate = {
            "data": {
                "rating": 6,
                "comment": "produtao bom :D",
                "product_id": 1
            },
            "response": {
                "rating": [
                    "Ensure this value is less than or equal to 5."
                ]
            }
        }

        cls.rating_unregistred_products = {
            "data": {
                "rating": 5,
                "comment": "produtao bom :D",
                "product_id": 2
            },
            "response": {
                "detail": "Not found."
            }
        }

    def test_product_rating(self):
        client = APIClient()

        client.post('/api/accounts/', self.staff['accounts'], format='json')
        client.post('/api/accounts/', self.user['accounts'], format='json')

        staff_login = client.post(
            '/api/login/', self.staff['login'], format='json')
        staff_token = staff_login.json()['token']

        user_login = client.post(
            '/api/login/', self.user['login'], format='json')
        user_token = user_login.json()['token']

        client.credentials(HTTP_AUTHORIZATION=f'Token {staff_token}')
        client.post('/api/products/', self.product_data, format='json')

        rate_with_staff_token = client.post(
            '/api/rating/', self.rate['data'], format='json')

        client.credentials(HTTP_AUTHORIZATION=f'Token {user_token}')

        rate_with_user_token = client.post(
            '/api/rating/', self.rate['data'], format='json'
        )

        rate_with_incorrect_fields = client.post(
            '/api/rating/', self.rate_wrong_fields['data'], format='json'
        )

        rate_with_six_stars = client.post(
            '/api/rating/', self.six_stars_rate['data'], format='json'
        )

        rating_unregistred_products = client.post(
            '/api/rating/',
            self.rating_unregistred_products['data'],
            format='json'
        )

        self.assertEquals(rate_with_staff_token.status_code, 403)
        self.assertEquals(rate_with_user_token.status_code, 201)
        self.assertDictEqual(
            rate_with_user_token.json(), self.rate['response'])

        self.assertEquals(rate_with_incorrect_fields.status_code, 400)
        self.assertDictEqual(
            rate_with_incorrect_fields.json(),
            self.rate_wrong_fields['response'])

        self.assertEquals(rate_with_six_stars.status_code, 400)
        self.assertDictEqual(
            rate_with_six_stars.json(), self.six_stars_rate['response'])

        self.assertEquals(rating_unregistred_products.status_code, 404)
        self.assertDictEqual(
            rating_unregistred_products.json(),
            self.rating_unregistred_products['response'])
