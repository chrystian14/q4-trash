from django.shortcuts import get_list_or_404, get_object_or_404

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication

from ecommerce.services import BUILD_HTTP_RESPONSE
from product.models import Product

from .serializers import (
    RatingPatchSerializer, RatingSerializer,
    RatingGetSerializer, RatingPostSerializer)
from .permissions import OnlyLoggedUserCanRate
from .models import Rate


class RatingView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [OnlyLoggedUserCanRate]

    def get(self, request, path_id: int = None):
        if not path_id:
            return Response(**BUILD_HTTP_RESPONSE(status=404))

        rated_product = get_list_or_404(Rate, product_id=path_id)
        serializer = RatingSerializer(rated_product, many=True).data
        response = RatingGetSerializer(many=True).to_representation(serializer)

        return Response(**BUILD_HTTP_RESPONSE(data=response, status=200))

    def post(self, request, path_id=None):
        if path_id:
            return Response(**BUILD_HTTP_RESPONSE(status=422))

        data = request.data
        serializer = RatingSerializer(data=data)

        if not serializer.is_valid():
            return Response(**BUILD_HTTP_RESPONSE(
                data=serializer.errors, status=400))

        product = get_object_or_404(Product, id=data.get('product_id'))

        data['product_id'] = product
        data['product'] = data.pop('product_id')

        Rate.objects.create(**data, user=request.user)

        response = RatingPostSerializer().to_representation(data, product)

        return Response(**BUILD_HTTP_RESPONSE(data=response, status=201))

    def patch(self, request, path_id=None):
        if not path_id:
            return Response(**BUILD_HTTP_RESPONSE(status=404))

        serializer = RatingPatchSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(**BUILD_HTTP_RESPONSE(
                data=serializer.errors, status=400))

        rate = get_object_or_404(Rate, id=path_id)
        rate.comment = request.data['comment']
        rate.rating = request.data['rating']
        rate.save()

        response = serializer.data
        response['id'] = path_id

        return Response(**BUILD_HTTP_RESPONSE(data=response, status=200))

    def delete(self, request, path_id=None):
        if not path_id:
            return Response(**BUILD_HTTP_RESPONSE(status=404))

        rate = get_object_or_404(Rate, id=path_id)
        if rate.user_id != request.user.id:
            message = "You do not have permission to perform this action."
            return Response(**BUILD_HTTP_RESPONSE(
                data={"detail": message}, status=403))

        rate.delete()

        return Response(**BUILD_HTTP_RESPONSE(status=204))
