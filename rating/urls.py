from django.urls import path
from .views import RatingView


urlpatterns = [
    path('rating/', RatingView.as_view()),
    path('rating/<int:path_id>/', RatingView.as_view())
]
