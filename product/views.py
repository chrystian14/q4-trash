from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication

from category.models import Category
from inventory.models import Inventory
from ecommerce.services import BUILD_HTTP_RESPONSE

from .models import Product
from .serializers import ProductSerializer
from .permissions import ProductPermission
from django.core.exceptions import ObjectDoesNotExist


class ProductView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [ProductPermission]

    def get(self, request):
        products = Product.objects.all()
        serializer = ProductSerializer(products, many=True)

        return Response(**BUILD_HTTP_RESPONSE(
            data=serializer.data, status=200))

    def post(self, request):
        categories = request.data['categories']

        serializer = ProductSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(**BUILD_HTTP_RESPONSE(
                data=serializer.errors, status=400))

        product, created = Product.objects.get_or_create(
            name=request.data['name'], description=request.data['description'],
            price=request.data['price'])

        if not created:
            return Response(**BUILD_HTTP_RESPONSE(
                data={'message': f'{product.name} already exists'},
                status=422))

        for category in categories:
            category = Category.objects.get_or_create(**category)[0]
            product.categories.add(category)

        # Pressupõe-se que o produto nao está no estoque a prior
        Inventory.objects.create(product=product)

        serializer = ProductSerializer(product)

        return Response(**BUILD_HTTP_RESPONSE(
            data=serializer.data, status=201))

    def delete(self, request, product_id=0):
        try:
            product = Product.objects.get(id=product_id)
        except ObjectDoesNotExist:
            return Response(**BUILD_HTTP_RESPONSE(status=404))

        self.check_object_permissions(request, product)

        product.delete()

        return Response(**BUILD_HTTP_RESPONSE(status=204))
