from django.test import TestCase
from rest_framework.test import APIClient

from product.models import Product
from category.models import Category
from inventory.models import Inventory
import ipdb


class TestProductView(TestCase):
    def setUp(self):
        self.admin = {
            "accounts": {
                "username": "admin",
                "password": "1234",
                "is_staff": True,
                "is_superuser": True
            },
            "login": {
                "username": "admin",
                "password": "1234"
            }
        }

        self.seller = {
            "accounts": {
                "username": "seller",
                "password": "1234",
                "is_staff": True
            },
            "login": {
                "username": "seller",
                "password": "1234"
            }
        }

        self.user = {
            "accounts": {
                "username": "client",
                "password": "1234"
            },
            "login": {
                "username": "client",
                "password": "1234"
            }
        }
        self.product_1_data = {
            'name': 'A Piada Mortal',
            'price': 29.90,
            'description': 'HQ Piada Mortal',
            'categories': [
                {
                    'name': 'Drama',
                    'description': 'Dramático'
                },
                {
                    'name': 'Graphic Novel',
                    'description': 'Histórias fechadas'
                }
            ]
        }

        self.product_1_should_be = {
            'id': 1,
            'name': 'A Piada Mortal',
            'price': 29.90,
            'description': 'HQ Piada Mortal',
            'categories': [
                {
                    'id': 1,
                    'name': 'Drama',
                    'description': 'Dramático'
                },
                {
                    'id': 2,
                    'name': 'Graphic Novel',
                    'description': 'Histórias fechadas'
                }
            ]
        }

    def test_seller_can_create_product(self):
        client = APIClient()

        # Create admin, log in, get admin token to create seller
        admin = client.post(
            '/api/accounts/', self.admin['accounts'], format='json')
        adm_token = client.post(
            '/api/login/', self.admin['login'], format='json').json()['token']
        client.credentials(HTTP_AUTHORIZATION=f'Token {adm_token}')

        # Create seller, log in, get seller token to create product
        seller = client.post(
            '/api/accounts/', self.seller['accounts'], format='json')
        seller_token = client.post(
            '/api/login/', self.seller['login'], format='json').json()['token']

        product = client.post(
            '/api/products/', self.product_1_data, format='json')

        # status code test
        self.assertEquals(product.status_code, 201)
        # dict test
        self.assertTrue(product.json(), self.product_1_should_be)

    def test_user_can_create_product(self):
        client = APIClient()

        # Create user, log in, get user token to create product
        user = client.post(
            '/api/accounts/', self.user['accounts'], format='json')
        user_token = client.post(
            '/api/login/', self.user['login'], format='json').json()['token']

        client.credentials(HTTP_AUTHORIZATION=f'Token {user_token}')

        product = client.post(
            '/api/products/', self.product_1_data, format='json')

        # status code test, should be 403 - Unauthorized
        self.assertEquals(product.status_code, 403)

    def test_anyone_can_get_products(self):
        client = APIClient()

        # Create admin, log in, get admin token to create seller
        admin = client.post(
            '/api/accounts/', self.admin['accounts'], format='json')
        adm_token = client.post(
            '/api/login/', self.admin['login'], format='json').json()['token']
        client.credentials(HTTP_AUTHORIZATION=f'Token {adm_token}')

        product = client.post(
            '/api/products/', self.product_1_data, format='json')

        product_list = client.get('/api/products/').json()

        # Authenticated user can get products
        self.assertEqual(product_list, [self.product_1_should_be])

        # Non authenticated user can get products
        client = APIClient()
        product_list = client.get('/api/products/').json()

        self.assertEqual(product_list, [self.product_1_should_be])
