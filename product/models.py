from django.db import models
from category.models import Category


class Product(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    price = models.FloatField()
    description = models.TextField()

    # inventory_id = models.ForeignKey(Inventory, on_delete=models.CASCADE)
    categories = models.ManyToManyField(Category, related_name='products')

    def __str__(self):
        return f'{self.id} - {self.name}'
