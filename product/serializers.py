from rest_framework import serializers
from category.serializers import CategorySerializer


class ProductSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(max_length=255)
    price = serializers.FloatField()
    description = serializers.CharField(max_length=255)

    categories = CategorySerializer(many=True, read_only=True)
