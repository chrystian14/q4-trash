from django.shortcuts import render, get_list_or_404

from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from .models import Inventory
from .permissions import InventoryPermission
from .serializers import InventorySerializer, InventoryGetSerializer
from ecommerce.services import BUILD_HTTP_RESPONSE


class InventoryView(APIView):
    """ Responsável pelo gerenciamento de rotas do inventário

    Args:
        APIView (APIView): Herda de APIView para controle de requisições

    Returns:
        Response: Dados serializados e código de status
    """
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated, InventoryPermission]

    def get(self, request):
        inventory = get_list_or_404(Inventory)
        serializer = InventorySerializer(inventory, many=True).data

        response = InventoryGetSerializer(
            many=True).to_representation(serializer)

        return Response(**BUILD_HTTP_RESPONSE(data=response, status=200))

    def put(self, request):
        serializer = InventorySerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        inventory = Inventory.objects.get(
            product_id=request.data['product_id'])
        inventory.quantity = request.data['quantity']
        inventory.save()

        serializer = InventorySerializer(inventory)

        return Response(serializer.data, status=status.HTTP_200_OK)
