from django.db import models
from product.models import Product


class Inventory(models.Model):
    id = models.AutoField(primary_key=True)
    quantity = models.IntegerField(default=0)

    product = models.OneToOneField(
        Product, related_name='inventory', on_delete=models.CASCADE)

    def __str__(self):
        return f'ID: {self.id} - QTD: {self.quantity}'
