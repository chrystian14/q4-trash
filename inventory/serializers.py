from rest_framework import serializers
from product.serializers import ProductSerializer
from product.models import Product


class InventorySerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    quantity = serializers.IntegerField()

    product_id = serializers.IntegerField()


class InventoryGetSerializer(serializers.Serializer):

    def to_representation(self, data: dict):
        product = Product.objects.get(id=data['product_id'])
        product_serializer = ProductSerializer(product).data

        return {
            'id': data['id'],
            'name': product_serializer['name'],
            'price': product_serializer['price'],
            'description': product_serializer['description'],
            'quantity': data['quantity']
        }
