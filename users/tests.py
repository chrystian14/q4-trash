from django.test import TestCase

from rest_framework.test import APIClient


class AccountsViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.accounts_respose = ('id', 'username', 'is_superuser', 'is_staff',)
        cls.login_response = ('token',)

        cls.admin = {
            "accounts": {
                "username": "admin",
                "password": "1234",
                "is_staff": True,
                "is_superuser": True
            },
            "login": {
                "username": "admin",
                "password": "1234"
            }
        }

        cls.seller = {
            "accounts": {
                "username": "seller",
                "password": "1234",
                "is_staff": True
            },
            "login": {
                "username": "seller",
                "password": "1234"
            }
        }

        cls.user = {
            "accounts": {
                "username": "client",
                "password": "1234"
            },
            "login": {
                "username": "client",
                "password": "1234"
            }
        }

        cls.incorrect_fields = {
            "name": "client1",
            "pass": "1234"
        }

    def test_login_accounts_view(self):
        client = APIClient()

        admin = client.post(
            '/api/accounts/', self.admin['accounts'], format='json')
        user = client.post(
            '/api/accounts/', self.user['accounts'], format='json')
        seller_without_token = client.post(
            '/api/accounts/', self.seller['accounts'], format='json')
        incorrect_fields = client.post(
            '/api/accounts/', self.incorrect_fields, format='json')

        self.assertEquals(admin.status_code, 201)
        self.assertEquals(tuple(admin.data.keys()), self.accounts_respose)

        self.assertEquals(user.status_code, 201)
        self.assertEquals(tuple(user.data.keys()), self.accounts_respose)

        self.assertEquals(seller_without_token.status_code, 401)

        self.assertEquals(incorrect_fields.status_code, 400)
        self.assertEquals(
            tuple(incorrect_fields.data.keys()), ('username', 'password',))

        admin_login = client.post(
            '/api/login/', self.admin['login'], format='json')
        admin_token = admin_login.json()['token']

        user_login = client.post(
            '/api/login/', self.user['login'], format='json')
        user_token = user_login.json()['token']

        client.credentials(HTTP_AUTHORIZATION=f'Token {user_token}')

        seller_with_client_token = client.post(
            '/api/accounts/', self.seller['accounts'], format='json')

        seller_login = client.post(
            '/api/login/', self.seller['login'], format='json')

        self.assertEquals(admin_login.status_code, 200)
        self.assertEquals(tuple(admin_login.data.keys()), self.login_response)

        self.assertEquals(user_login.status_code, 200)
        self.assertEquals(tuple(user_login.data.keys()), self.login_response)

        self.assertEquals(seller_with_client_token.status_code, 403)

        client.credentials(HTTP_AUTHORIZATION=f'Token {admin_token}')

        seller = client.post(
            '/api/accounts/', self.seller['accounts'], format='json')

        seller_login = client.post(
            '/api/login/', self.seller['login'], format='json')

        self.assertEquals(seller.status_code, 201)
        self.assertEquals(tuple(seller.data.keys()), self.accounts_respose)

        self.assertEquals(seller_login.status_code, 200)
        self.assertEquals(tuple(seller_login.data.keys()), self.login_response)

        same_username = client.post(
            '/api/accounts/', self.admin['accounts'], format='json')

        incorrect_fields2 = client.post(
            '/api/login/', self.incorrect_fields, format='json')

        self.assertEquals(same_username.status_code, 422)
        self.assertEquals(
            same_username.data, {'message': 'User already exists!'})

        self.assertEquals(incorrect_fields2.status_code, 400)
        self.assertEquals(
            tuple(incorrect_fields2.data.keys()), ('username', 'password',))
