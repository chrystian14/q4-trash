from rest_framework.permissions import BasePermission


class OnlyAdminCanCreateSeller(BasePermission):
    def has_permission(self, request, view):
        is_staff = request.data.get('is_staff')
        is_superuser = request.data.get('is_superuser')

        not_admin = not is_staff and not is_superuser
        is_admin = is_staff and is_superuser

        if not_admin or is_admin:
            return True

        if request.user.is_superuser and is_staff:
            return True

        return False
