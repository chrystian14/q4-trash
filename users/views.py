from django.db import IntegrityError
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication

from ecommerce.services import BUILD_HTTP_RESPONSE
from .serializers import UserSerializer, LoginSerializer
from .permissions import OnlyAdminCanCreateSeller


class AccountsView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [OnlyAdminCanCreateSeller]

    def post(self, request):
        serializer = UserSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(**BUILD_HTTP_RESPONSE(
                data=serializer.errors, status=400))

        try:
            user = User.objects.create_user(**request.data)
            serializer = UserSerializer(user)

            return Response(**BUILD_HTTP_RESPONSE(
                data=serializer.data, status=201))

        except IntegrityError:
            return Response(**BUILD_HTTP_RESPONSE(
                data={'message': "User already exists!"}, status=422))


class LoginView(APIView):
    def post(self, request):
        serializer = LoginSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(**BUILD_HTTP_RESPONSE(
                serializer.errors, status=400))

        user = authenticate(
            username=request.data['username'],
            password=request.data['password']
        )

        if user is not None:
            token = Token.objects.get_or_create(user=user)[0]
            return Response(**BUILD_HTTP_RESPONSE(
                data={'token': token.key}, status=200))

        return Response(**BUILD_HTTP_RESPONSE(status=401))
