from rest_framework import serializers


class UserSerializer(serializers.Serializer):
    id = serializers.PrimaryKeyRelatedField(read_only=True)

    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True, write_only=True)

    is_superuser = serializers.BooleanField(required=False)
    is_staff = serializers.BooleanField(required=False)


class LoginSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)

    username = serializers.CharField()
    password = serializers.CharField(write_only=True)

    is_staff = serializers.BooleanField(read_only=True)
    is_superuser = serializers.BooleanField(read_only=True)
